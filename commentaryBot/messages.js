import { messages } from './mockData';
import { partialGetRandomInt } from '../helpers';

// Factory Pattern

class Message {
  constructor(name) {
    this.name = name;
    this.messagesNumber = this.getMessagesNumber();
    this.messageInd = this.getMessageInd();
  }

  getMessage() {

  }

  getMessageInd() {
    return partialGetRandomInt(this.messagesNumber - 1) || 0;
  }

  getMessagesNumber() {
    return messages[this.name].length
  }
}

export class HelloMessage extends Message {
  constructor() {
    super('hello');
  }

  getMessage() {
    return messages[this.name][this.messageInd].staticPart;
  }
}

export class StartMessage extends Message {
  constructor(usersCars) {
    super('start');
    this.usersCars = usersCars;
  }

  getMessage() {
    const messageObj = messages[this.name][this.messageInd];

    let message = messageObj.staticPart;

    Object.keys(this.usersCars).forEach((user, ind) => {
      message += messageObj.getDynamicPart(user, this.usersCars[user], ind + 1)
    })

    return message
  }
}

export class IntervalMessage extends Message {
  constructor(usersCars, usersProgress) {
    super('interval');
    this.usersCars = usersCars;
    this.usersProgress = usersProgress;
  }

  getMessage() {
    const messageObj = messages[this.name][this.messageInd];

    let message = messageObj.staticPart || '';

    const usersByProgress = Object
      .keys(this.usersProgress)
      .sort((a, b) => this.usersProgress[b] - this.usersProgress[a])

    usersByProgress.forEach((user, ind) => {
      message += messageObj.getDynamicPart(user, this.usersCars[user], ind);
    })

    return message.slice(0, -2) + '.'
  }
}

export class BeforeFinishMessage extends Message {
  constructor(user, car) {
    super('beforeFinish');
    this.user = user;
    this.car = car;
  }

  getMessage() {
    const messageObj = messages[this.name][this.messageInd];
    const message = messageObj.getDynamicPart(this.user, this.car);

    return message
  }
}

export class FinishMessage extends Message {
  constructor(user) {
    super('finish');
    this.user = user;
  }

  getMessage() {
    const messageObj = messages[this.name][this.messageInd];
    const message = messageObj.getDynamicPart(this.user);

    return message
  }
}

export class EndMessage extends Message {
  constructor(finishedUsersTime, unfinishedUsersProgress, secondsForGame) {
    super('end');
    this.finishedUsersTime = finishedUsersTime;
    this.unfinishedUsersProgress = unfinishedUsersProgress;
    this.secondsForGame = secondsForGame;
  }

  getMessage() {
    const messageObj = messages[this.name][this.messageInd];
    let message = messageObj.staticPart;

    let finishedUsers = Object.keys(this.finishedUsersTime);

    if (finishedUsers.length > 1) {
      finishedUsers.sort((a, b) => this.finishedUsersTime[a] - this.finishedUsersTime[b]);
    }

    if (finishedUsers.length < 3) {
      const unfinishedUsers = Object.keys(this.unfinishedUsersProgress)
        .sort((a, b) => this.unfinishedUsersProgress[b] - this.unfinishedUsersProgress[a]);

      finishedUsers.push(...unfinishedUsers.slice(0, 3 - finishedUsers.length));
    }

    finishedUsers.forEach((user, ind) => {
      const time = this.finishedUsersTime[user] || this.secondsForGame;
      message += messageObj.getDynamicPart(user, time, ind);
    })

    return message.slice(0, -2) + '.';
  }
}