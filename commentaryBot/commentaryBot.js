import { SEC_MESSAGE_INTERVAL, CHARS_TO_FINISH } from './config';
import {
  BeforeFinishMessage,
  EndMessage,
  FinishMessage,
  HelloMessage,
  IntervalMessage,
  StartMessage
} from './messages';
import { cars } from './mockData';
import { partialGetRandomInt } from '../helpers';

const MS_IN_ONE_SEC = 1000;

export class CommentaryBot {
  constructor(io, usersProgress, fullLength, roomName) {
    this.io = io;
    this.usersProgress = usersProgress;
    this.users = this.getUsers();
    this.usersCars = this.getUsersCars();
    this.fullLength = fullLength;
    this.roomName = roomName;
    this.isSentBeforeFinish = false;
    this.finishedUsersTime = {};
    this.unfinishedUsersProgress = this.usersProgress;
    this.startTime = 0;
    this.secondsForGame = 0;
  }

  start(secondsBeforeStartGame, secondsForGame) {
    this.secondsForGame = secondsForGame;

    this.io.in(this.roomName).emit('MESSAGE', new HelloMessage().getMessage());

    setTimeout(() => {
      const message = new StartMessage(this.usersCars).getMessage();

      this.startTime = Date.now();

      this.io.in(this.roomName).emit('MESSAGE', message);

      this.timer = setInterval(() => {
        const message = new IntervalMessage(this.usersCars, this.usersProgress).getMessage();

        if (this.hasUsersProgress()) {
          this.io.in(this.roomName).emit('MESSAGE', message);
        }
      }, MS_IN_ONE_SEC * SEC_MESSAGE_INTERVAL)

    }, MS_IN_ONE_SEC * secondsBeforeStartGame);
  }

  update(usersProgress) {
    this.usersProgress = usersProgress;

    if (!this.isSentBeforeFinish && this.isLeftNumCharsToFinish()) {
      const userBeforeFinish = Object
        .keys(usersProgress)
        .find(user => usersProgress[user] === this.fullLength - CHARS_TO_FINISH);

      const car = this.usersCars[userBeforeFinish];

      const message = new BeforeFinishMessage(userBeforeFinish, car).getMessage();

      this.io.in(this.roomName).emit('MESSAGE', message);

      this.isSentBeforeFinish = true;
    }

    if (this.isNewFinished()) {
      const finishedUser = Object
        .keys(this.unfinishedUsersProgress)
        .find(unfinishedUser => usersProgress[unfinishedUser] === this.fullLength);

      delete this.unfinishedUsersProgress[finishedUser];

      this.finishedUsersTime[finishedUser] = (Date.now() - this.startTime) / MS_IN_ONE_SEC;

      if (this.users.length === Object.keys(this.finishedUsersTime).length) {
       this.end();
      } else {
        const message = new FinishMessage(finishedUser).getMessage();

        this.io.in(this.roomName).emit('MESSAGE', message);
      }
    }
  }

  end() {
    const message = new EndMessage(
      this.finishedUsersTime,
      this.unfinishedUsersProgress,
      this.secondsForGame
    ).getMessage();

    this.io.in(this.roomName).emit('MESSAGE', message);

    clearInterval(this.timer);
  }

  isLeftNumCharsToFinish() {
    return Object
      .values(this.usersProgress)
      .find(progress => progress === this.fullLength - CHARS_TO_FINISH)
  }

  isNewFinished() {
    return Object
      .values(this.unfinishedUsersProgress)
      .find(progress => progress === this.fullLength)
  }

  hasUsersProgress() {
    return Object.values(this.usersProgress).find(progress => progress > 0);
  }

  getUsers() {
    return Object.keys(this.usersProgress);
  }

  getUsersCars() {
    const usersCars = {};

    this.users.forEach(user => {
      usersCars[user] = this.getRandomCar();
    })

    return usersCars;
  }

  getRandomCar() {
    const randomInt = partialGetRandomInt(cars.length - 1);

    return cars[randomInt];
  }
}
