import { createElement, addClass, removeClass, removeSpaces, alertMessage } from './helpers.mjs';
import { createRoomHandler, backToRooms, readyHandler } from './eventHandlers.mjs';
import { createRoom, addRoom } from './components/room.mjs';
import { createUserBlock } from './components/userBlock.mjs';

const username = sessionStorage.getItem('username');

export const roomsPage = document.getElementById('rooms-page');
export const gamePage = document.getElementById('game-page');
export const roomsContainer = document.getElementById('rooms-container');
export const readyBtn = document.getElementById('ready-btn');
export const commentator = document.getElementById('commentator');
const createRoomButton = document.getElementById('create-room-button');
const userList = document.getElementById('user-list');
const backToRoomsBtn = document.getElementById('back-to-rooms');
const gameScreen = document.getElementById('game-screen');
const comment = document.getElementById('comment');

let charIndex = 0;
let onKeyDownHandler;
let timerId;

if (!username) {
  window.location.replace('/login');
}

export const socket = io('http://localhost:3002', { query: { username } });

backToRoomsBtn.addEventListener('click', backToRooms);
readyBtn.addEventListener('click', readyHandler);
createRoomButton.addEventListener('click', createRoomHandler);

const onUsernameError = message => {
  alertMessage(message);

  sessionStorage.removeItem('username');

  window.location.replace('/login');
}

const updateRoomsList = rooms => {
  const allRooms = rooms.map(createRoom);

  roomsContainer.append(...allRooms);
}

const joinRoomDone = ({ roomName, roomUsers, readyUsers }) => {
  const h1 = gamePage.querySelector('h1');

  const allUserBlocks = roomUsers.map(createUserBlock.bind(null, username, readyUsers));

  h1.textContent = roomName;
  readyBtn.textContent = 'Ready';
  userList.innerHTML = '';

  userList.append(...allUserBlocks);

  addClass(roomsPage, 'display-none');
  removeClass(gamePage, 'display-none');
}

const updateUsersNumber = ({ roomName, length }) => {
  const currentRoom = document.getElementById(removeSpaces(roomName));

  if (currentRoom) {
    const p = currentRoom.querySelector('p');
    p.textContent = length > 1 ? `${length} users connected` : `1 user connected`;
  }
}

const deleteRoomFromList = roomName => {
  const roomItem = document.getElementById(removeSpaces(roomName));

  if (roomItem) {
    roomItem.remove();
  }
}

const userExitFromRoom = username => {
  document.getElementById(username).remove();
}

const newUserInRoom = username => {
  const newUserBlock = createUserBlock(null, [], username);
  userList.append(newUserBlock);
}

const updateUserReadyStatus = username => {
  const usernameBlock = document.getElementById(username + '-name');
  usernameBlock.classList.contains('ready')
    ? removeClass(usernameBlock, 'ready')
    : addClass(usernameBlock, 'ready')
}

const updateProgressIndicator = ({ username, percentageWidth }) => {
  const indicator = document.getElementById(username).querySelector('.indicator');
  indicator.style.width = percentageWidth + '%';
  if (percentageWidth === 100) {
    addClass(indicator, 'lime');
  }
}

const keyDownHandler = (text, gameText, indicator, event) => {
  if (event.key === text[charIndex]) {
    charIndex = charIndex + 1;
    const textDone = `<span class="text-done">${text.slice(0, charIndex)}</span>`;
    const nextChar = `<span class="next-char">${text[charIndex] || ''}</span>`
    const textUndone = `<span>${text.slice(charIndex + 1)}</span>`;
    const percentageWidth = charIndex / text.length * 100;

    socket.emit('UPDATE_USER_PROGRESS', { progressLength: charIndex, fullLength: gameText.length });

    gameText.innerHTML = textDone + nextChar + textUndone;
    indicator.style.width = percentageWidth + '%';

    if (percentageWidth === 100) {
      addClass(indicator, 'lime');
    }

    if (charIndex >= text.length) {
      document.removeEventListener('keydown', onKeyDownHandler);
      socket.emit('USER_FINISHED');
    }
  }
}

const timeLeftCallback = (i, SECONDS_FOR_GAME, timeLeft) => {
  timeLeft.textContent = `${SECONDS_FOR_GAME - i} seconds left`;

  if (SECONDS_FOR_GAME - i > 0) {
    i++;

    timerId = setTimeout(timeLeftCallback.bind(null, i, SECONDS_FOR_GAME, timeLeft), 1000)
  } else {
    document.removeEventListener('keydown', onKeyDownHandler);

    socket.emit('TIME_OUT');
  }
}

const startGame = (text, SECONDS_FOR_GAME, timer) => {
  let i = 1;
  const gameText = createElement({
    tagName: 'div',
    className: 'game-text',
    attributes: {id: 'game-text'},
    textContent: text
  });
  const timeLeft = createElement({
    tagName: 'span',
    className: 'time-left',
    attributes: {
      id: 'time-left'
    },
    textContent: `${SECONDS_FOR_GAME} seconds left`
  })

  const indicator = document.getElementById(username).querySelector('.indicator');

  timer.remove();

  onKeyDownHandler =  keyDownHandler.bind(null, text, gameText, indicator);
  document.addEventListener('keydown', onKeyDownHandler);

  gameScreen.append(timeLeft, gameText);

  timerId = setTimeout(timeLeftCallback.bind(null, i, SECONDS_FOR_GAME, timeLeft), 1000);
}

const startTimer = ({ SECONDS_TIMER_BEFORE_START_GAME, randomId, SECONDS_FOR_GAME }) => {
  let i = 1;

  backToRoomsBtn.style.visibility = 'hidden';
  readyBtn.style.display = 'none';
  comment.textContent = '';

  const timer = createElement({
    tagName: 'span',
    className: 'game-timer',
    attributes: { id: 'game-timer' },
    textContent: String(SECONDS_TIMER_BEFORE_START_GAME)
  })
  gameScreen.append(timer);

  removeClass(commentator, 'display-none');

  const timeoutCallback = () =>{
    timer.textContent = String(SECONDS_TIMER_BEFORE_START_GAME - i);
    if (SECONDS_TIMER_BEFORE_START_GAME - i > 0) {
      setTimeout(timeoutCallback, 1000)
    } else {
      fetch(`/game/texts/${randomId}`)
        .then(response => response.json())
        .then(data => startGame(data.text, SECONDS_FOR_GAME, timer))
        .catch(e => console.log(e))
    }
    i++;
  }

  setTimeout(timeoutCallback, 1000)
}

const showMessage = message => {
  comment.textContent = message;
}

const finishGame = () => {
  const usernameBlocks = document.querySelectorAll('.user-name');
  const indicators = document.querySelectorAll('.indicator');
  charIndex = 0;

  clearTimeout(timerId);

  document.getElementById('time-left').remove();
  document.getElementById('game-text').remove();

  backToRoomsBtn.style.visibility = 'visible';
  readyBtn.textContent = 'Ready';
  readyBtn.style.display = 'block';

  indicators.forEach(indicator => {
    if (indicator.style.width === '100%') {
      removeClass(indicator, 'lime');
    }
    indicator.style.width = '0';
  })

  usernameBlocks.forEach(usernameBlock => {
    removeClass(usernameBlock, 'ready');
  })
}

socket.on('USERNAME_ERROR', onUsernameError);
socket.on('ROOMNAME_ERROR', alertMessage);
socket.on('MAX_USERS_ERROR', alertMessage);

socket.on('ADD_ROOM', addRoom);
socket.on('JOIN_ROOM_DONE', joinRoomDone);

socket.on('UPDATE_ROOMS_LIST', updateRoomsList);
socket.on('UPDATE_USERS_NUMBER', updateUsersNumber);
socket.on('DELETE_ROOM_FROM_LIST', deleteRoomFromList);

socket.on('NEW_USER_IN_ROOM', newUserInRoom);
socket.on('USER_EXIT_FROM_ROOM', userExitFromRoom);
socket.on('UPDATE_USER_READY_STATUS', updateUserReadyStatus);
socket.on('UPDATE_PROGRESS_INDICATOR', updateProgressIndicator);

socket.on('START_TIMER', startTimer);
socket.on('FINISH_GAME', finishGame);

socket.on('MESSAGE', showMessage);

