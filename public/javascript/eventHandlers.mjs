import { addClass, removeClass } from './helpers.mjs';
import { socket, gamePage, roomsPage, readyBtn, commentator } from './game.mjs';

export const createRoomHandler = () => {
  let roomName = prompt('Input room name:');

  if (!roomName) return;

  if (roomName.length > 10) {
    roomName = roomName.slice(0, 10);
  }
  roomName = roomName.toLowerCase();
  socket.emit('CREATE_ROOM', roomName);
}

export const backToRooms = () => {
  socket.emit('DELETE_USER_FROM_ROOM');

  removeClass(roomsPage, 'display-none');
  addClass(gamePage, 'display-none');
  addClass(commentator, 'display-none');
}

export const readyHandler = () => {
  socket.emit('USER_READY');

  readyBtn.textContent === 'Ready'
    ? readyBtn.textContent = 'Not ready'
    : readyBtn.textContent = 'Ready';
}


