import { addClass, createElement } from '../helpers.mjs';

export const createUserBlock = (username, readyUsers, user) => {
  const userBlock = createElement({
    tagName: 'div',
    className: 'user-block',
    attributes: { id: user }
  })
  const usernameBlock = createElement({
    tagName: 'div',
    className: 'user-name',
    attributes: { id: user + '-name' },
    textContent: user === username ? user + ' (you)' : user
  })
  const progressBar = createElement({
    tagName: 'div',
    className: 'progress-bar'
  })
  const indicator = createElement({
    tagName: 'div',
    className: 'indicator'
  })

  if (readyUsers.find(readyUser => readyUser === user)) {
    addClass(usernameBlock, 'ready');
  }

  progressBar.append(indicator);
  userBlock.append(usernameBlock, progressBar);

  return userBlock;
}