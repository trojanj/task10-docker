import { createElement, removeSpaces } from '../helpers.mjs';
import { socket, roomsContainer } from '../game.mjs';

export const createRoom = roomName => {
  const roomId = removeSpaces(roomName);
  const roomItem = createElement({
    tagName: 'div',
    className: 'room-item',
    attributes: { id: roomId }
  });
  const p = createElement({ tagName: 'p', textContent: '1 user connected' });
  const h2 = createElement({ tagName: 'h2', textContent: roomName });
  const button = createElement({ tagName: 'button', className: 'btn no-select', textContent: 'Join' });

  const onJoinRoom = () => {
    socket.emit('JOIN_ROOM', roomName);
  };

  button.addEventListener('click', onJoinRoom);

  roomItem.append(p, h2, button);

  return roomItem;
}

export const addRoom = ({ roomName, socketId }) => {
  const room = createRoom(roomName);

  roomsContainer.append(room);

  if (socketId) {
    socket.emit('JOIN_ROOM', roomName);
  }
}