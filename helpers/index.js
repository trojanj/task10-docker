export const getRandomInt = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export const partialGetRandomInt = max => {
  const min = 0;
  return getRandomInt(min, max)
}

export const excludeFromArr = (arr, excludedArr) => {
  if (excludedArr.length) {
    return arr.filter(room => !excludedArr.includes(room))
  }
  return arr
}