import { MAXIMUM_USERS_FOR_ONE_ROOM, SECONDS_FOR_GAME, SECONDS_TIMER_BEFORE_START_GAME } from './config';
import { texts } from '../data';
import { excludeFromArr, getRandomInt } from '../helpers';
import { CommentaryBot } from '../commentaryBot/commentaryBot';

const activeUsersMap = new Map(); // [user, room]
const roomsMap = new Map(); // [room, [users...]]
const readyUsersMap = new Map(); // [room, [readyUsers...]]
const activeRooms = [];
const finishedUsersMap = new Map(); // [room, [finishedUsers...]]
const usersProgressMap = new Map(); // [room, {username: progressLength}]
const commentaryBots = {}; // {room: new commentaryBot()}

export default io => {
  io.on("connect", socket => {
    const username = socket.handshake.query.username;

    if (activeUsersMap.has(username)) {
      const message = 'Such username already exists. Please choose another one.';
      socket.emit('USERNAME_ERROR', message);
    } else if (username !== 'null') {
      activeUsersMap.set(username, null);
    }

    if (roomsMap.size) {
      let rooms = [ ...roomsMap.keys() ];
      rooms = excludeFromArr(rooms, activeRooms);

      socket.emit('UPDATE_ROOMS_LIST', rooms);
    }

    socket.on('CREATE_ROOM', roomName => {
      if (roomsMap.has(roomName)) {
        const message = 'Room with such name already exists.';
        socket.emit('ROOMNAME_ERROR', message);
        return;
      }

      roomsMap.set(roomName, []);
      readyUsersMap.set(roomName, []);

      socket.broadcast.emit('ADD_ROOM', {roomName});
      socket.emit('ADD_ROOM', {roomName, socketId: socket.id});
    })

    socket.on('JOIN_ROOM', roomName => {
      const roomUsers = roomsMap.get(roomName);

      if (roomUsers.length === MAXIMUM_USERS_FOR_ONE_ROOM) {
        const message = 'Maximum users in the room. Please choose another one.'
        socket.emit('MAX_USERS_ERROR', message);
        return;
      }

      const readyUsers = readyUsersMap.get(roomName);
      roomUsers.push(username);

      roomsMap.set(roomName, roomUsers);
      activeUsersMap.set(username, roomName);

      if (roomUsers.length === MAXIMUM_USERS_FOR_ONE_ROOM) {
        activeRooms.push(roomName);

        io.emit('DELETE_ROOM_FROM_LIST', roomName);
      }

      socket.join(roomName, () => {
        socket.emit('JOIN_ROOM_DONE', { roomName, roomUsers, readyUsers });
        socket.to(roomName).emit('NEW_USER_IN_ROOM', username);

        if (roomUsers.length > 1) {
          io.emit('UPDATE_USERS_NUMBER', { roomName, length: roomUsers.length })
        }
      });
    })

    socket.on('DELETE_USER_FROM_ROOM', () => {
      deleteUserFromRoom(io, socket, username);
    })

    socket.on('USER_READY', () => {
      const roomName = activeUsersMap.get(username);
      let readyUsers = readyUsersMap.get(roomName);

      const isRoomFitToStartGame = () => {
        return readyUsers.length === roomsMap.get(roomName).length && activeRooms.includes(roomName);
      };

      if (readyUsers.includes(username)) {
        readyUsers = readyUsers.filter(user => user !== username);
        readyUsersMap.set(roomName, readyUsers);
      } else {
        readyUsers.push(username);
        readyUsersMap.set(roomName, readyUsers);
      }

      io.to(roomName).emit('UPDATE_USER_READY_STATUS', username);

      if (isRoomFitToStartGame()) {
        const randomId = getRandomInt(0, texts.length - 1);
        const progressData = {};

        readyUsers.forEach(readyUser => {
          progressData[readyUser] = 0;
        })

        usersProgressMap.set(roomName, progressData);

        io.in(roomName).emit('START_TIMER', {
          SECONDS_TIMER_BEFORE_START_GAME,
          randomId, SECONDS_FOR_GAME
        });

        const usersProgress = usersProgressMap.get(roomName);
        const fullLength = texts[randomId].length;

        commentaryBots[roomName] = new CommentaryBot(io, usersProgress, fullLength, roomName);
        commentaryBots[roomName].start(SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME);
      }
    })

    socket.on('UPDATE_USER_PROGRESS', ({ progressLength, fullLength }) => {
      const roomName = activeUsersMap.get(username);
      const usersProgress = usersProgressMap.get(roomName);

      const percentageWidth = progressLength / fullLength * 100;

      usersProgress[username] = progressLength;
      usersProgressMap.set(roomName, usersProgress);

      commentaryBots[roomName].update(usersProgress);

      socket.to(roomName).emit('UPDATE_PROGRESS_INDICATOR', ({ username, percentageWidth }))
    })

    socket.on('USER_FINISHED', () => {
      const roomName = activeUsersMap.get(username);
      const finishedUsers = finishedUsersMap.get(roomName) || [];
      const roomUsersNumber = roomsMap.get(roomName).length;

      finishedUsers.push(username);

      finishedUsersMap.set(roomName, finishedUsers)

      if (finishedUsers.length === roomUsersNumber) {
        readyUsersMap.set(roomName, []);
        finishedUsersMap.delete(roomName);
        usersProgressMap.delete(roomName);

        delete commentaryBots[roomName];

        io.in(roomName).emit('FINISH_GAME');
      }
    })

    socket.on('TIME_OUT', () => {
      const roomName = activeUsersMap.get(username);

      if (commentaryBots[roomName]) {
        commentaryBots[roomName].end();
      }

      readyUsersMap.set(roomName, []);
      finishedUsersMap.delete(roomName);
      usersProgressMap.delete(roomName);

      delete commentaryBots[roomName];

      socket.emit('FINISH_GAME');
    })

    socket.on('disconnect', () => {
      deleteUserFromRoom(io, socket, username);
      activeUsersMap.delete(username);
    })
  });
};

function deleteUserFromRoom(io, socket, username) {
  const roomName = activeUsersMap.get(username);

  if (roomName) {
    const roomUsers = roomsMap.get(roomName).filter(user => user !== username);

    socket.leave(roomName);

    activeUsersMap.set(username, null);

    if (!roomUsers.length) {
      roomsMap.delete(roomName);
      readyUsersMap.delete(roomName);

      io.emit('DELETE_ROOM_FROM_LIST', roomName);
    } else {
      const readyUsers = readyUsersMap.get(roomName);

      roomsMap.set(roomName, roomUsers);
      readyUsersMap.set(roomName, readyUsers.filter(readyUser => readyUser !== username));

      io.to(roomName).emit('USER_EXIT_FROM_ROOM', username);
      io.emit('UPDATE_USERS_NUMBER', { roomName, length: roomUsers.length });
    }
  }
}
