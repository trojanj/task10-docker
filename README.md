Task #10 - Docker  

### Start project using docker
1. `docker run --rm -it -p 3002:3002 trojanj/keyboard-racing`

or  

1. `git clone https://trojanj@bitbucket.org/trojanj/task10-docker.git`
2. `cd task10-docker`
2. `docker-compose up -d`

