FROM node:12-alpine

WORKDIR /app

RUN npm install -g nodemon

COPY . .

RUN npm install

EXPOSE 3002

CMD ["nodemon", "server.js", "--exec", "\"babel-node\""]

